# README #

This package adds a laravel artisan command for pulling a remote mysql database down, and importing it over your existing database. It's very useful for syncing your dev environment with your staging environment.

### How do I get set up? ###

1. Install the package with composer
2. Add the package to the providers array in config/app.php

Database\DatabasePullServiceProvider::class,

3. Set your .env variables

STAGING_DB_HOST=

STAGING_DB_USERNAME=

STAGING_DB_DATABASE=

STAGING_DB_PASSWORD=

STAGING_DB_PORT=

### Usage ###

php artisan db:pull