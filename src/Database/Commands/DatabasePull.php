<?php

namespace Database\Commands;

use Illuminate\Console\Command;
/**
 * This class runs on php artisan database:pull
 * it dumps the staging database into the local wordpress
 * database so the user has the most up to date data to work
 * with.
 * 
 * @author Dan Sudenfield <Dan@alipes.com>
 */
class DatabasePull extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:pull';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dumps a database from .env STAGING_DB to the local wordpress database';

    // location of sql dump file
    const DB_FILE_PATH = 'tmp/sqlDump.sql';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command. This checks to see if it has the STAGING_DB env
     * variables. if it does then it creates a tmp folder and sql dumps the
     * staging database to a file in tmp. Then it imports that database
     * to the local machine so the user has an up to date database.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!getenv('STAGING_DB_HOST') || !getenv('STAGING_DB_USERNAME') || !getenv('STAGING_DB_DATABASE') || !getenv('STAGING_DB_PASSWORD') || !getenv('STAGING_DB_PORT')) {
            $this->error('The .env file does not contain the correct environment variables');
        }

        if (!file_exists(storage_path('tmp'))) {
            // path does not exist
            mkdir(storage_path('tmp'));
        }
        $this->comment('Creating local database');
        $this->exec('mysql -u ' . getenv('DB_USERNAME') . ' -p' . getenv('DB_PASSWORD') . ' -e "CREATE DATABASE IF NOT EXISTS ' . getenv('DB_DATABASE') . '"'); 
        $this->comment('Local database created');
        $this->comment('Dumping foreign database to file');
        // dump database from staging on aws to file locally
        $this->exec('mysqldump -h ' .  getenv('STAGING_DB_HOST') . ' -u ' .  getenv('STAGING_DB_USERNAME') . ' -p' . getenv('STAGING_DB_PASSWORD') . ' --port='. getenv('STAGING_DB_PORT') .  ' ' . getenv('STAGING_DB_DATABASE') . ' > ' . storage_path(self::DB_FILE_PATH));
        $this->comment('Foreign database dumped to file');
        $this->comment('Importing database');
        // import database from local file
        $this->exec('mysql -h ' . getenv('DB_HOST') . ' -u ' . getenv('DB_USERNAME') . ' -p' . getenv('DB_PASSWORD')  . ' '. getenv('DB_DATABASE') . ' < ' . storage_path(self::DB_FILE_PATH));
        $this->comment('Database Imported');
        // remove sql dump file
        unlink(storage_path(self::DB_FILE_PATH));
    }

    public function exec($cmd)
    {
        exec($cmd, $results);
        if (count($results))
        {
            $this->comment($results[0]);
        }
        return $results;
    }
}

