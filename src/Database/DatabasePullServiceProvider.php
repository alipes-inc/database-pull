<?php
namespace Database;
use Illuminate\Support\ServiceProvider;

class DatabasePullServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                'Database\Commands\DatabasePull',
            ]);
        }
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}